#!/usr/bin/env bash

echo "copying extra files"

MNTDIR="mnt"

echo "making a copy of the interface to the RasperBerryPi"
cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0

echo "create a folder to place the repository"
mkdir $MNTDIR/home/pi/Documents

echo "clone the main Projects repository codes "
git clone git@gitlab.com:21s-itt2-datacenter-students-group/team-b3.git
cp -r -v mqtt $MNTDIR/home/pi/Documents/mqtt
