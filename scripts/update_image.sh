#!/usr/bin/env bash

# ths is needed for some proot specific issue
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/sbin:/sbin

echo adding gwma28853 ssh key to pi user
mkdir -p /home/pi/.ssh
curl -s https://gitlab.com/gwma28853.keys > /home/pi/.ssh/authorized_keys
chown pi:pi /home/pi/.ssh -R

echo update apt and install stuff
apt-get update
apt-get install -y git python3-pip

echo install python packages
pip3 install pyserial pyyaml paho-mqtt digi-xbee

echo "enable ssh at boot time"
touch /boot/ssh

echo 'make serial available (maybe)'
usermod -a -G dialout pi

echo 'ensure that the serial port is not used by linux'
sed -i.bak 's/console=serial0,115200 //' boot/cmdline.txt

echo "enable uart"
echo "enable_uart=1" >> /boot/config.txt
